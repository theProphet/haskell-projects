type N = Integer

infixr 0 #
(#) = id

main :: IO ()
main = do
  print # h 0 5 7
  print # h 1 5 7
  print # h 2 3 4
  print # h 3 2 4

h :: N -> N -> N -> N
h 0 a 0 = a
h 0 a b = 1 + h 0 a (b - 1)
h 1 a 0 = 0
h n a 0 = 1
h n a b = h (n - 1) a (h n a (b - 1))