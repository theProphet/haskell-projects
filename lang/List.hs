module List
  ( List
  , nil
  , cons
  , list_exa
  , list_foldr
  ) where

import qualified Prelude as P
import Func

data List a = Nil | Cons a (List a)
  deriving (P.Eq, P.Ord, P.Show)

nil :: List a
nil = Nil

cons :: a -> List a -> List a
cons = Cons

list_exa :: (k -> r) -> (a -> List a -> (k -> r) -> k -> r) -> k -> List a -> r
list_exa z f k Nil = z k
list_exa z f k (Cons x xs) = f x xs (flip (list_exa z f) xs) k

---------------------------------------------------------------------------------------------------------------------------------------

list_foldr :: (a -> r -> r) -> r -> List a -> r
list_foldr f = list_exa id (\x xs fxs k -> f x (fxs k))