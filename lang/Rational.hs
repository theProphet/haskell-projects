module Rational
  ( Rational
  -- , int_mk_nat
  -- , int_mk_neg
  -- , int_exa
  -- , int_lit
  ) where

import qualified Prelude as P

import Nat
import Int

data Rational = Rational Int Nat
  deriving (P.Eq, P.Ord, P.Show)

-- rat_ :: Nat -> Int
-- int_mk_nat = IntNat
-- 
-- int_mk_neg :: Nat -> Int
-- int_mk_neg = IntNeg
-- 
-- int_exa :: (Nat -> a) -> (Nat -> a) -> Int -> a
-- int_exa f g (IntNat n) = f n
-- int_exa f g (IntNeg n) = g n
-- 
-- int_lit :: (P.Num a, P.Ord a) => a -> Int
-- int_lit n = if (P.>=) n 0
--   then int_mk_nat (nat_lit n)
--   else int_mk_neg (nat_lit ((P.-) (P.abs n) 1))

---------------------------------------------------------------------------------------------------------------------------------------