{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
module Examples.P00001.Test where
import qualified Prelude as P
import Core
import PList

test :: [PList '[String]]
test =
  [ str_lit "Hello, World!" # PNil
  ]