module Nat
  ( Nat
  , nat_zero
  , nat_succ
  , nat_exa
  , nat_lit
  , nat_for
  ) where

import qualified Prelude as P
import Func
import Unit

data Nat = Zero | Succ Nat
  deriving (P.Eq, P.Ord, P.Show)

nat_zero :: Nat
nat_zero = Zero

nat_succ :: Nat -> Nat
nat_succ = Succ

nat_exa :: (k -> r) -> (Nat -> (k -> r) -> k -> r) -> k -> Nat -> r
nat_exa z f k Zero = z k
nat_exa z f k (Succ n) = f n (flip (nat_exa z f) n) k

nat_lit :: (P.Num a, P.Eq a) => a -> Nat
nat_lit 0 = nat_zero
nat_lit n = nat_succ (nat_lit ((P.-) n 1))

---------------------------------------------------------------------------------------------------------------------------------------

nat_for :: (r -> r) -> r -> Nat -> r
nat_for f = nat_exa id (\n fn k -> f (fn k))