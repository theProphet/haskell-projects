{-# LANGUAGE TypeFamilies, MultiParamTypeClasses, FlexibleInstances, UndecidableInstances #-}

module ZipWith (zipWith) where
import Prelude hiding (zipWith)
import qualified Prelude as P

type family A a where
  A (a -> b) = [a] -> A b
  A a = [a]

class (A f ~ a) => C f a where
  m :: [f] -> A f

instance (A f ~ [f]) => C f [f] where
  m = id

instance (b' ~ A b, C b b', f ~ (a -> b)) => C f ([a] -> b') where
  m = (m.) . P.zipWith id

zipWith :: (a' ~ A a, C a a') => a -> a'
zipWith = m . repeat