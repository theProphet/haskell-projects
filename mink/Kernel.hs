module Kernel
  ( module KernelBase
  
  , Expr(..)
  , Equality(..)
  , Lemma(..)
  , Goal(..)
  , ProofT(..)
  , Proof
  , Theory
  , Asm
  
  , build_theory
  , build_proof
  , apply_tac
  , define
  , get_proof
  , set_proof
  , get
  , gets
  , get_goal
  , get_asms
  , get_asm
  , get_goal_eq
  , get_asms_num
  , count_expr
  , flip_eq
  , negate_eq
  , get_vars
  , get_goals_num
  , expr_has_var
  , is_var
  , get_name
  , is_ctor
  , same_ctor
  , get_call_info
  , exprs_to_call
  , parse_and_apply_def
  , parse_and_apply_defs
  
  , tac_clear
  , tac_nth_rewrite_asm
  , tac_nth_rewrite_goal
  , tac_rewrite_asm
  , tac_rewrite_goal
  , tac_cases
  , tac_split
  , tac_cases_expr
  , tac_nth_unfold_asm
  , tac_nth_unfold_goal
  , tac_refl
  , tac_def
  , tac_have
  , tac_focus_goal
  , tac_swap_asms
  , tac_nub_asms
  , tac_contrapose
  , tac_dsimp_asm
  , tac_dsimp_goal
  , tac_norm_asm
  , tac_norm_goal
  , tac_purge_vars
  , tac_induction
  , tac_sort_goals
  ) where

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import KernelBase

newtype State a b = State (a -> (b, a))

instance Functor (State a) where
  fmap f (State s) = State # \th -> let
    ~(a, th1) = s th
    in (f a, th1)

instance Applicative (State a) where
  pure a = State # \th -> (a, th)
  liftA2 f (State s1) (State s2) = State # \th -> let
    ~(a, th1) = s1 th
    ~(b, th2) = s2 th1
    in (f a b, th2)

instance Monad (State a) where
  State s >>= f = State # \th -> let
    ~(a, th1) = s th
    ~(State g) = f a
    in g th1

instance MonadFail (State a) where
  fail = error

run_state :: State s a -> s -> (a, s)
run_state (State f) z = f z

eval_state :: State s a -> s -> a
eval_state s z = fst # run_state s z

exec_state :: State s a -> s -> s
exec_state s z = snd # run_state s z

type Theory = State TheoryT
type Proof = State ProofT

build_theory :: Theory () -> TheoryT
build_theory (State f) = snd # f empty_theory

get_proof :: TheoryT -> ProofT
get_proof th = fromJust # _proof th

set_proof :: ProofT -> Theory ()
set_proof proof = modify # \th -> th {_proof = Just proof}

parse_and_apply_defs :: String -> Theory ()
parse_and_apply_defs str = mapM_ parse_and_apply_def # filter (not . null) #
  map (unwords . words) # lines str

parse_and_apply_def :: String -> Theory ()
parse_and_apply_def str = let
  [lhs, rhs] = split_at_elem '=' str
  (name:args) = words lhs
  vars = Set.fromList args
  body = put_vars_expr vars # read rhs
  in define name args body

get :: State a a
get = State # \s -> (s, s)

put :: a -> State a ()
put s = State # \s1 -> ((), s)

gets :: (a -> b) -> State a b
gets f = do
  s <- get
  return # f s

modify :: (a -> a) -> State a ()
modify f = do
  s <- get
  put # f s

expr_has_ref :: Name -> Expr -> Prop
expr_has_ref name expr = case expr of
  Pair a b -> expr_has_ref name a || expr_has_ref name b
  Other a -> expr_has_ref name a
  Call a b -> expr_has_ref name a || expr_has_ref name b
  Ref s -> s == name
  _ -> False

expr_has_var :: Name -> Expr -> Prop
expr_has_var name expr = case expr of
  Pair a b -> expr_has_var name a || expr_has_var name b
  Other a -> expr_has_var name a
  Call a b -> expr_has_var name a || expr_has_var name b
  Var s -> s == name
  _ -> False

has_def :: Name -> Theory Prop
has_def name = do
  defs <- gets _defs
  return # Map.member name defs

get_def_th :: Name -> Theory (Maybe Def)
get_def_th name = do
  defs <- gets _defs
  return # fromJust # Map.lookup name defs

set_def :: Name -> Maybe Def -> Theory ()
set_def name def = modify # \th ->
  th {_defs = Map.insert name def # _defs th}

declare :: Name -> Theory ()
declare name = do
  has <- has_def name
  if has
    then error # concat ["Constant ", show name, " has already been declared"]
    else set_def name Nothing

define :: Name -> [Name] -> Expr -> Theory ()
define name names_list expr = do
  True <- pure # names_list == nub names_list
  let names = Set.fromList names_list
  declare name
  ok <- check_names names expr
  let is_rec = expr_has_ref name expr
  if ok
    then do
      let args = Map.fromList # zip names_list [0..]
      define' # Def
        { _def_name = name
        , _def_arity = fi # length names_list
        , _def_args_list = names_list
        , _def_args = args
        , _body = expr
        , _is_rec = is_rec
        }
    else error # concat ["Invalid definition for ", show name]

define' :: Def -> Theory ()
define' def = do
  let name = _def_name def
  has <- has_def name
  if has
    then do
      d <- get_def_th name
      case d of
        Nothing -> do
          set_def name # Just def
          modify # \th -> th {_defs_list = _defs_list th ++ [(name, Just def)]}
        Just _ -> error # concat ["Constant ", show name, " has already been defined"]
    else error # concat ["Constant ", show name, " must be declared first"]

check_names :: Set Name -> Expr -> Theory Prop
check_names names expr = case expr of
  Nil -> pure True
  Pair a b -> check_names2 names a b
  Other a -> check_names names a
  Call a b -> check_names2 names a b
  Var s -> pure # Set.member s names
  Ref name -> do
    has <- has_def name
    if has then pure True else
      error # concat ["Undefined constant ", show name]

check_names2 :: Set Name -> Expr -> Expr -> Theory Prop
check_names2 names a b = do
  c1 <- check_names names a
  c2 <- check_names names b
  return # c1 && c2

check_names_eq :: Set Name -> Equality -> Theory Prop
check_names_eq names (Equality p a b) = check_names2 names a b

check_lemma :: Lemma -> Theory Prop
check_lemma lemma = allM (check_names_eq # _lemma_vars lemma) # lemma_to_eqs lemma

apply_tac :: Proof () -> ProofT -> ProofT
apply_tac = exec_state

build_proof :: Lemma -> Proof () -> Theory ProofT
build_proof lemma (State f) = do
  ok <- check_lemma lemma
  if ok
    then do
      th <- get
      return # snd # f # init_proof th lemma
    else error "Invalid lemma statement"

get_goal :: Proof Goal
get_goal = do
  (x:xs) <- gets _goals
  return # x

pop_goal :: Proof Goal
pop_goal = do
  (x:xs) <- gets _goals
  modify # \p -> p {_goals = xs}
  return x

close_goal :: Proof ()
close_goal = do
  pop_goal
  return ()

set_goal :: Goal -> Proof ()
set_goal goal = do
  (x:xs) <- gets _goals
  modify # \p -> p {_goals = goal : xs}

get_asm :: N -> Proof Asm
get_asm n = do
  goal <- get_goal
  return # _asms goal !! fi n

set_asm :: N -> Asm -> Proof ()
set_asm n asm = do
  goal <- get_goal
  let asms = _asms goal
  set_goal # goal {_asms = set_at n asm asms}

get_goal_eq :: Proof Equality
get_goal_eq = do
  goal <- get_goal
  return # _goal_eq goal

modify_goal :: (Goal -> Goal) -> Proof ()
modify_goal f = do
  goal <- get_goal
  set_goal # f goal

tac_clear :: N -> Proof ()
tac_clear i = modify_goal # \goal ->
  goal {_asms = remove_at i # _asms goal}

data RewriteT = RewriteT
  { _rw_find :: Expr
  , _rw_replace :: Expr
  , _rw_n :: N
  , _rw_all :: Prop
  , _rw_done :: Prop
  }

type Rewrite = State RewriteT

nth_rewrite :: Equality -> Expr -> Expr -> N -> Equality
nth_rewrite asm a b n = rewrite' asm a b # Just n

rewrite :: Equality -> Expr -> Expr -> Equality
rewrite asm a b = rewrite' asm a b Nothing

rewrite' :: Equality -> Expr -> Expr -> Maybe N -> Equality
rewrite' asm a b mn = eval_state (rewrite_eq asm) # RewriteT
  { _rw_find = a
  , _rw_replace = b
  , _rw_n = maybe 0 id mn
  , _rw_all = isNothing mn
  , _rw_done = False
  }

rw_if_not_done :: (a -> Rewrite a) -> a -> Rewrite a
rw_if_not_done f a = do
  done <- gets _rw_done
  if done then pure a else f a

rewrite_eq :: Equality -> Rewrite Equality
rewrite_eq = rw_if_not_done # \eq -> do
  let Equality p lhs rhs = eq
  lhs1 <- rewrite_expr lhs
  rhs1 <- rewrite_expr rhs
  info <- get
  if _rw_all info || _rw_done info
    then return # Equality p lhs1 rhs1
    else error "rewrite"

rewrite_expr :: Expr -> Rewrite Expr
rewrite_expr = rw_if_not_done # \expr -> do
  info <- get
  if expr == _rw_find info
    then do
      let n = _rw_n info
      if _rw_all info || n == 0
        then do
          put # info {_rw_done = not # _rw_all info}
          return # _rw_replace info
        else do
          put # info {_rw_n = n - 1}
          return expr
    else do
      case expr of
        Pair a b -> do
          a1 <- rewrite_expr a
          b1 <- rewrite_expr b
          return # Pair a1 b1
        Other a -> do
          a1 <- rewrite_expr a
          return # Other a1
        Call a b -> do
          a1 <- rewrite_expr a
          b1 <- rewrite_expr b
          return # Call a1 b1
        _ -> pure expr

tac_nth_rewrite_asm :: N -> N -> N -> Proof ()
tac_nth_rewrite_asm src_i target_i n = modify_goal # \goal -> let
  asms = _asms goal
  Equality True a b = asms !! fi src_i
  eq = nth_rewrite (asms !! fi target_i) a b n
  in goal {_asms = set_at target_i eq asms}

tac_nth_rewrite_goal :: N -> N -> Proof ()
tac_nth_rewrite_goal src_i n = modify_goal # \goal -> let
  asms = _asms goal
  Equality True a b = asms !! fi src_i
  eq = nth_rewrite (_goal_eq goal) a b n
  in goal {_goal_eq = eq}

tac_rewrite_asm :: N -> N -> Proof ()
tac_rewrite_asm src_i target_i = modify_goal # \goal -> let
  asms = _asms goal
  Equality True a b = asms !! fi src_i
  eq = rewrite (asms !! fi target_i) a b
  in goal {_asms = set_at target_i eq asms}

tac_rewrite_goal :: N -> Proof ()
tac_rewrite_goal src_i = modify_goal # \goal -> let
  asms = _asms goal
  Equality True a b = asms !! fi src_i
  eq = rewrite (_goal_eq goal) a b
  in goal {_goal_eq = eq}

type Asm = Equality

is_ctor :: Expr -> Prop
is_ctor Nil = True
is_ctor (Pair _ _) = True
is_ctor (Other _) = True
is_ctor _ = False

same_ctor :: Expr -> Expr -> Prop
same_ctor Nil Nil = True
same_ctor (Pair _ _) (Pair _ _) = True
same_ctor (Other _) (Other _) = True
same_ctor _ _ = False

tac_cases :: N -> Proof ()
tac_cases i = do
  goal <- get_goal
  let asms = _asms goal
  let Equality True a b = asms !! fi i
  asms <- pure # remove_at i asms
  modify_goal # \goal -> goal {_asms = asms}
  if is_ctor a && is_ctor b
    then case (a, b) of
      (Nil, Nil) -> pure ()
      (Pair a b, Pair c d) -> do
        let eqs = [Equality True a c, Equality True b d]
        modify_goal # \goal -> goal {_asms = append_at i eqs asms}
      (Other a, Other b) -> do
        let eq = Equality True a b
        modify_goal # \goal -> goal {_asms = insert_at i eq asms}
      _ -> close_goal
    else error "tac_cases"

tac_split :: Proof ()
tac_split = do
  goal <- get_goal
  let Equality True a b = _goal_eq goal
  pop_goal
  if is_ctor a && is_ctor b
    then case (a, b) of
      (Nil, Nil) -> pure ()
      (Pair a b, Pair c d) -> do
        let eqs = [Equality True a c, Equality True b d]
        let goals = map (\eq -> goal {_goal_eq = eq}) eqs
        modify # \p -> p {_goals = goals ++ _goals p}
      (Other a, Other b) -> do
        let eq = Equality True a b
        modify # \p -> p {_goals = goal {_goal_eq = eq} : _goals p}
    else error "tac_cases"

get_vars :: Proof (Set Name)
get_vars = do
  goal <- get_goal
  return # _goal_vars goal

get_goals_num :: Proof N
get_goals_num = do
  goals <- gets _goals
  return # fi # length goals

check_expr :: Expr -> Proof ()
check_expr expr = do
  th <- gets _theory
  vars <- get_vars
  let ok = eval_state (check_names vars expr) th
  if ok then pure () else error "Invalid expression"

check_eq :: Equality -> Proof ()
check_eq (Equality p a b) = do
  check_expr a
  check_expr b

all_vars :: [Name]
all_vars = map (:[]) ['a'..]

get_avail_var :: Set Name -> Name
get_avail_var = get_avail all_vars

get_avail_var' :: Set Name -> (Name, Set Name)
get_avail_var' = get_avail' all_vars

new_var :: Proof Name
new_var = do
  goal <- get_goal
  let vars = _goal_vars goal
  let v = get_avail_var vars
  set_goal # goal {_goal_vars = Set.insert v vars}
  return v

tac_cases_expr :: Expr -> Proof ()
tac_cases_expr expr = do
  check_expr expr
  goal <- pop_goal
  let id0 = _id goal
  let asms = _asms goal
  let mk_eq = Equality True expr
  let vars = _goal_vars goal
  let (v1, vars1) = get_avail_var' vars
  let (v2, vars2) = get_avail_var' vars1
  goals <- pure #
    [ goal {_id = id0 ++ [0], _asms = asms ++ [mk_eq Nil]}
    , goal {_id = id0 ++ [1], _goal_vars = vars2, _asms = asms ++ [mk_eq # Pair (Var v1) (Var v2)]}
    , goal {_id = id0 ++ [2], _goal_vars = vars1, _asms = asms ++ [mk_eq # Other (Var v1)]}
    ]
  modify # \p -> p {_goals = goals ++ _goals p}

get_def :: Name -> Proof (Maybe Def)
get_def name = do
  th <- gets _theory
  let defs = _defs th
  return # fromJust # Map.lookup name defs

get_call_info :: Expr -> (Expr, [Expr])
get_call_info expr = let
  (x : xs) = reverse # get_call_info' expr
  in (x, xs)

get_call_info' :: Expr -> [Expr]
get_call_info' (Call a b) = b : get_call_info' a
get_call_info' a = [a]

exprs_to_call :: [Expr] -> Expr
exprs_to_call = foldl1 Call

modify_asm :: N -> (Equality -> Proof Equality) -> Proof ()
modify_asm n f = do
  goal <- get_goal
  let asms = _asms goal
  eq1 <- f # asms !! fi n
  set_goal # goal {_asms = set_at n eq1 asms}

modify_goal_eq :: (Equality -> Proof Equality) -> Proof ()
modify_goal_eq f = do
  goal <- get_goal
  eq1 <- f # _goal_eq goal
  set_goal # goal {_goal_eq = eq1}

unfold_using_call_info :: N -> Map Name N -> Expr -> [Expr] -> Expr
unfold_using_call_info arity args_map body args = let
  (xs, ys) = splitAt (fi arity) args
  xs1 = Map.map (\n -> xs !! fi n) args_map
  expr1 = subst_args xs1 body
  in exprs_to_call # expr1 : ys

data NthUnfoldT = NthUnfoldT
  { _unf_name :: Name
  , _unf_arity :: N
  , _unf_args :: Map Name N
  , _unf_body :: Expr
  , _unf_n :: N
  , _unf_done :: Prop
  }

type NthUnfold = State NthUnfoldT

tac_nth_unfold_asm :: Name -> N -> N -> Proof ()
tac_nth_unfold_asm name asm_i n = modify_asm asm_i # nth_unfold name n

tac_nth_unfold_goal :: Name -> N -> Proof ()
tac_nth_unfold_goal name n = modify_goal_eq # nth_unfold name n

nth_unfold :: Name -> N -> Equality -> Proof Equality
nth_unfold name n eq = do
  Just def <- get_def name
  return # eval_state (nth_unfold_eq eq) # NthUnfoldT
    { _unf_name = name
    , _unf_arity = _def_arity def
    , _unf_args = _def_args def
    , _unf_body = _body def
    , _unf_n = n
    , _unf_done = False
    }

unf_if_not_done :: (a -> NthUnfold a) -> a -> NthUnfold a
unf_if_not_done f a = do
  done <- gets _unf_done
  if done then pure a else f a

nth_unfold_eq :: Equality -> NthUnfold Equality
nth_unfold_eq = unf_if_not_done # \eq -> do
  let Equality p lhs rhs = eq
  lhs1 <- nth_unfold_expr lhs
  rhs1 <- nth_unfold_expr rhs
  done <- gets _unf_done
  if done
    then return # Equality p lhs1 rhs1
    else error "unfold"

nth_unfold_expr :: Expr -> NthUnfold Expr
nth_unfold_expr = unf_if_not_done # \expr -> do
  let (target, args) = get_call_info expr
  name <- gets _unf_name
  if target == Ref name
    then do
      n <- gets _unf_n
      if n == 0
        then do
          modify # \info -> info {_unf_done = True}
          arity <- gets _unf_arity
          args_map <- gets _unf_args
          body <- gets _unf_body
          True <- pure # fi arity <= length args
          return # unfold_using_call_info arity args_map body args
        else do
          modify # \info -> info {_unf_n = n - 1}
          ys <- mapM nth_unfold_expr args
          return # exprs_to_call # target : ys
    else case expr of
      Pair a b -> do
        a1 <- nth_unfold_expr a
        b1 <- nth_unfold_expr b
        return # Pair a1 b1
      Other a -> do
        a1 <- nth_unfold_expr a
        return # Other a1
      Call a b -> do
        a1 <- nth_unfold_expr a
        b1 <- nth_unfold_expr b
        return # Call a1 b1
      _ -> pure expr

subst_args_eq :: Map Name Expr -> Equality -> Equality
subst_args_eq args (Equality p a b) = Equality p
  (subst_args args a) (subst_args args b)

subst_args :: Map Name Expr -> Expr -> Expr
subst_args args expr = case expr of
  Pair a b -> Pair (subst_args args a) (subst_args args b)
  Other a -> Other (subst_args args a)
  Call a b -> Call (subst_args args a) (subst_args args b)
  Var name -> case Map.lookup name args of
    Nothing -> error # name
    Just expr -> expr
  _ -> expr

tac_refl :: Proof ()
tac_refl = do
  Equality True a b <- get_goal_eq
  if a == b then close_goal else error "tac_refl"

add_asm :: Asm -> Proof ()
add_asm asm = do
  modify_goal # \goal -> goal {_asms = _asms goal ++ [asm]}

add_goal :: Goal -> Proof ()
add_goal goal = do
  modify # \p -> p {_goals = goal : _goals p}

tac_def :: Expr -> Proof ()
tac_def expr = do
  check_expr expr
  n <- new_var
  add_asm # Equality True (Var n) expr

tac_have :: Equality -> Proof ()
tac_have eq = do
  check_eq eq
  goal <- get_goal
  let id0 = _id goal
  set_goal # goal {_id = id0 ++ [1], _asms = _asms goal ++ [eq]}
  add_goal # goal {_id = id0 ++ [0], _goal_eq = eq}

count_expr :: Expr -> Expr -> N
count_expr sub expr = if expr == sub then 1 else case expr of
  Pair a b -> count_expr sub a + count_expr sub b
  Other a -> count_expr sub a
  Call a b -> count_expr sub a + count_expr sub b
  _ -> 0

tac_focus_goal :: [N] -> Proof ()
tac_focus_goal goal_id = do
  goals <- gets _goals
  let Just n = fmap fi # findIndex (\goal -> _id goal == goal_id) goals
  let goal = goals !! fi n
  let goals1 = remove_at n goals
  modify # \p -> p {_goals = goal : goals1}

tac_swap_asms :: N -> N -> Proof ()
tac_swap_asms i j = modify_goal # \goal -> goal {_asms = swap i j # _asms goal}

tac_nub_asms :: Proof ()
tac_nub_asms = modify_goal # \goal -> goal {_asms = nub # _asms goal}

get_asms :: Proof [Asm]
get_asms = do
  goal <- get_goal
  return # _asms goal

get_asms_num :: Proof N
get_asms_num = do
  asms <- get_asms
  return # fi # length asms

flip_eq :: Equality -> Equality
flip_eq (Equality p a b) = Equality p b a

negate_eq :: Equality -> Equality
negate_eq (Equality p a b) = Equality (not p) a b

tac_contrapose :: N -> Proof ()
tac_contrapose n = do
  goal <- get_goal
  let asms = _asms goal
  let eq = _goal_eq goal
  set_goal # goal
    { _asms = set_at n (negate_eq eq) asms
    , _goal_eq = negate_eq # asms !! fi n }

tac_dsimp_asm :: N -> Proof ()
tac_dsimp_asm asm_i = modify_asm asm_i # lift dsimp_eq

tac_dsimp_goal :: Proof ()
tac_dsimp_goal = modify_goal_eq # lift dsimp_eq

dsimp_eq :: Equality -> Equality
dsimp_eq (Equality p a b) = Equality p (dsimp_expr a) (dsimp_expr b)

dsimp_expr :: Expr -> Expr
dsimp_expr expr = case expr of
  Call target arg -> let
    expr = Call (dsimp_expr target) (dsimp_expr arg)
    in case expr of
      Call (Call (Call Nil x) a) b -> case x of
        Nil -> a
        Pair _ _ -> b
        Other _ -> x
        _ -> expr
      Call (Pair a b) f -> dsimp_expr # Call (Call f a) b
      _ -> expr
  Pair a b -> Pair (dsimp_expr a) (dsimp_expr b)
  Other a -> Other (dsimp_expr a)
  _ -> expr

tac_norm_asm :: N -> Proof ()
tac_norm_asm asm_i = modify_asm asm_i norm_eq

tac_norm_goal :: Proof ()
tac_norm_goal = modify_goal_eq norm_eq

norm_eq :: Equality -> Proof Equality
norm_eq (Equality p a b) = do
  th <- gets _theory
  let defs = _defs th
  let a1 = norm_expr defs a
  let b1 = norm_expr defs b
  return # Equality p a1 b1

norm_expr :: Map Name (Maybe Def) -> Expr -> Expr
norm_expr defs expr = let
  (target, args) = get_call_info # dsimp_expr expr
  target1 = case target of
    Pair a b -> Pair (norm_expr defs a) (norm_expr defs b)
    Other a -> Other (norm_expr defs a)
    _ -> target
  rest = dsimp_expr # norm_expr' defs # target1 : args
  in case target1 of
    Ref name -> case fromJust # Map.lookup name defs of
      Just def -> let
        arity = _def_arity def
        args_map = _def_args def
        body = _body def
        in if _is_rec def || length args < fi arity then rest else
          dsimp_expr # norm_expr defs # unfold_using_call_info arity args_map body args
      Nothing -> rest
    _ -> rest

norm_expr' :: Map Name (Maybe Def) -> [Expr] -> Expr
norm_expr' defs (x:xs) = foldl1 Call # x : map (norm_expr defs) xs

get_used_vars_eqs :: [Equality] -> Set Name
get_used_vars_eqs eqs = foldr Set.union Set.empty # map get_used_vars_eq eqs

get_used_vars_eq :: Equality -> Set Name
get_used_vars_eq (Equality p a b) = get_used_vars2 a b

get_used_vars :: Expr -> Set Name
get_used_vars expr = case expr of
  Pair a b -> get_used_vars2 a b
  Other a -> get_used_vars a
  Call a b -> get_used_vars2 a b
  Var name -> Set.singleton name
  _ -> Set.empty

get_used_vars2 :: Expr -> Expr -> Set Name
get_used_vars2 a b = Set.union (get_used_vars a) (get_used_vars b)

tac_purge_vars :: Proof ()
tac_purge_vars = do
  goal <- get_goal
  let eqs = _goal_eq goal : _asms goal
  let vars = get_used_vars_eqs eqs
  set_goal # goal {_goal_vars = vars}

is_ref :: Expr -> Prop
is_ref (Ref _) = True
is_ref _ = False

is_var :: Expr -> Prop
is_var (Var _) = True
is_var _ = False

get_name :: Expr -> Name
get_name (Ref name) = name
get_name (Var name) = name

tac_induction :: N -> Proof ()
tac_induction n = do
  asms_num <- get_asms_num
  True <- pure # asms_num == 1
  
  Equality True expr rhs <- get_asm n
  let (Ref name, args) = get_call_info expr
  Just def <- get_def name
  let arity = _def_arity def
  
  True <- pure # rhs == 0
  True <- pure # length args == fi arity
  True <- pure # args == nub args
  True <- pure # all is_var args
  
  goal_orig <- get_goal
  let names_orig = map (\(Var a) -> a) args
  let goal_eq_orig = _goal_eq goal_orig
  
  let args_map = _def_args def
  let body = _body def
  let unf = unfold_using_call_info arity args_map body
  let expr = unf args
  let asm = Equality True expr rhs
  set_asm n asm
  tac_norm_asm n
  goal <- pop_goal
  
  get_new_goals <- pure # fix # \get_new_goals n goal -> let
    id0 = _id goal
    asms = _asms goal
    num = fi # length # asms
    Equality True lhs rhs' = asms !! fi n
    (target, args) = get_call_info lhs
    vars = _goal_vars goal
    in if not # expr_has_ref name lhs then [goal] else
      if target == Nil && length args == 3 then let
        [a, b, c] = args
        
        asm1 = Equality True b rhs'
        asm2 = Equality True a 0
        goal1 = goal {_id = id0 ++ [0], _asms = set_at n asm1 asms ++ [asm2]}
        
        (v1, vars1) = get_avail_var' vars
        (v2, vars2) = get_avail_var' vars1
        asm3 = Equality True c rhs'
        asm4 = Equality True a # Pair (Var v1) (Var v2)
        goal2 = goal {_id = id0 ++ [1], _goal_vars = vars2, _asms = set_at n asm3 asms ++ [asm4]}
        
        in nub # concatMap (concatMap (get_new_goals num) . nub . get_new_goals n) [goal1, goal2]
      else if target == Ref name && length args == fi arity then
        if rhs' == rhs then let
          args_map = Map.fromList # zip names_orig args
          asm1 = subst_args_eq args_map goal_eq_orig
          in [goal {_asms = set_at n asm1 asms}]
        else let
          -- asm1 = Equality True lhs rhs
          -- asm2 = Equality True rhs' rhs
          -- goal1 = goal {_id = id0 ++ [0], _asms = set_at n asm1 asms ++ [asm2]}
          -- 
          -- asm3 = Equality False rhs' rhs
          -- goal2 = goal {_id = id0 ++ [1], _asms = asms ++ [asm3]}
          -- 
          -- in nub # get_new_goals n goal1 ++ [goal2]
          in [goal]
      else [goal]
  
  let goals = get_new_goals n goal
  modify # \p -> p {_goals = goals ++ _goals p}

tac_sort_goals :: Proof ()
tac_sort_goals = do
  goals <- gets _goals
  let goals1 = zipWith (\goal i -> goal {_id = [i]}) (sort goals) [0..]
  modify # \p -> p {_goals = goals1}