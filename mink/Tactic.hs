module Tactic where

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Debug.Trace
import GHC.Read
import Text.ParserCombinators.ReadPrec

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import ParserBase
import Kernel

a = Var "a"
b = Var "b"
c = Var "c"
d = Var "d"
e = Var "e"
m = Var "m"
n = Var "n"

do_simp :: N
do_simp = 1

simp_after :: Proof () -> Proof ()
simp_after tac = do
  n1 <- get_goals_num
  tac
  n2 <- get_goals_num
  simp_n # max (n2 - n1 + 1) 0

refl :: Proof ()
refl = tac_refl

sp :: Proof ()
sp = simp_after tac_split

exact :: N -> Proof ()
exact i = do
  Equality p a b <- get_asm i
  if not p then cpos i >> exact i else do
    tac_nth_rewrite_goal i 0
    tac_refl

clear :: N -> Proof ()
clear = tac_clear

get_last_asm_i :: Proof N
get_last_asm_i = do
  n <- get_asms_num
  return # n - 1

symmetry :: N -> Proof ()
symmetry n = do
  eq <- get_asm n
  tac_have # flip_eq eq
  let cnt = count_expr (_lhs eq) (_rhs eq)
  tac_nth_rewrite_goal n cnt
  refl
  k <- get_last_asm_i
  tac_swap_asms n k
  clear k

nth_rw_at :: N -> N -> N -> Proof ()
nth_rw_at src_i asm_i n = tac_nth_rewrite_asm src_i asm_i n

nth_rw :: N -> N -> Proof ()
nth_rw src_i n = tac_nth_rewrite_goal src_i n

rw_at :: N -> N -> Proof ()
rw_at src_i asm_i = tac_rewrite_asm src_i asm_i

rw :: N -> Proof ()
rw src_i = tac_rewrite_goal src_i

unfold_at :: Name -> N -> Proof ()
unfold_at name n = simp_after #
  tac_nth_unfold_asm name n 0

unfold :: Name -> Proof ()
unfold name = simp_after #
  tac_nth_unfold_goal name 0

unf_at :: N -> Proof ()
unf_at n = do
  eq <- get_asm n
  let Just name = get_unf_target_name # _lhs eq
  unfold_at name n

unf :: Proof ()
unf = do
  eq <- get_goal_eq
  let Just name = get_unf_target_name # _lhs eq
  unfold name

get_unf_target_name :: Expr -> Maybe Name
get_unf_target_name expr = do
  let (target, args) = get_call_info expr
  case target of
    Ref name -> pure name
    _ -> msum # map get_unf_target_name args

dsimp_at :: N -> Proof ()
dsimp_at = tac_dsimp_asm

dsimp :: Proof ()
dsimp = tac_dsimp_goal

have :: Equality -> Proof ()
have = tac_have

cpos :: N -> Proof ()
cpos = tac_contrapose

cases :: N -> Proof ()
cases n = tac_cases n

contra :: Proof ()
contra = do
  have # Equality False 0 1
  have # Equality True Nil Nil
  refl
  cpos 0
  cases 0
  n <- get_last_asm_i
  cpos n

triv :: Proof ()
triv = refl

mk_eq :: Expr -> Expr -> Equality
mk_eq a b = Equality True a b

mk_eq_n :: Expr -> Expr -> Equality
mk_eq_n a b = Equality False a b

mk_prop :: Expr -> Equality
mk_prop expr = mk_eq expr 0

mk_prop_n :: Expr -> Equality
mk_prop_n expr = mk_eq_n expr 0

cases_expr :: Expr -> Proof ()
cases_expr expr = simp_after # do
  n <- get_asms_num
  tac_cases_expr expr
  goals <- gets _goals
  flip mapM_ (take 3 goals) # \goal -> focus goal >> subst' n

cases_var :: Name -> Proof ()
cases_var name = cases_expr # Var name

not_false :: Proof ()
not_false = do
  contra
  n <- get_last_asm_i
  cases n

elim :: N -> Proof ()
elim n = do
  contra
  exact n

norm_at :: N -> Proof ()
norm_at = tac_norm_asm

norm :: Proof ()
norm = tac_norm_goal

norm_all :: Proof ()
norm_all = do
  n <- get_last_asm_i
  mapM_ norm_at [0..n]
  norm

purge :: Proof ()
purge = tac_purge_vars

subst :: N -> Proof ()
subst n = simp_after # do
  subst' n
  clear n

subst' :: N -> Proof ()
subst' n = do
  num <- get_last_asm_i
  mapM_ (\k -> ite (k /= n) (rw_at n k) (pure ())) [0..num]
  rw n

induct :: N -> Proof ()
induct n = simp_after #
  tac_induction n

cases_ite :: N -> Proof ()
cases_ite n = do
  Equality True (Call (Call (Call Nil a) b) c) rhs <- get_asm n
  cases_expr a

split_ite :: Proof ()
split_ite = do
  Equality True (Call (Call (Call Nil a) b) c) rhs <- get_goal_eq
  cases_expr a

focus :: Goal -> Proof ()
focus goal = tac_focus_goal # _id goal

simp_n :: N -> Proof ()
simp_n num = do
  goals <- gets _goals
  simp_goals # take (fi num) goals

simp_goals :: [Goal] -> Proof ()
simp_goals = mapM_ simp_goal

is_ite :: Expr -> Prop
is_ite (Call (Call (Call Nil _) _) _) = True
is_ite _ = False

simp_rules :: [(Prop -> Expr -> Expr -> Prop, N -> Proof ())]
simp_rules =
  [ (\p a b -> p && a == b, \i -> clear i >> simp)
  , (\p a b -> p && is_var a && not (expr_has_var (get_name a) b), subst)
  , (\p a b -> p && is_var b && not (expr_has_var (get_name b) a), \i -> symmetry i >> simp)
  , (\p a b -> p && is_ctor a && is_ctor b && same_ctor a b, \i -> cases i >> simp)
  , (\p a b -> p && is_ctor a && is_ctor b, cases)
  , (\p a b -> p && is_ite a, cases_ite)
  , (\p a b -> not p && a == Nil && b == Nil, \i -> cpos i >> refl)
  ]

simp :: Proof ()
simp = do
  goals <- gets _goals
  if null goals then nop else get_goal >>= simp_goal

simp_goal :: Goal -> Proof ()
simp_goal goal = if do_simp == 0 then nop else do
  focus goal
  norm_all
  tac_nub_asms
  tac_purge_vars
  asms <- get_asms
  eq@(Equality p a b) <- get_goal_eq
  if p && a == b then refl else
    if p && is_pair a && is_pair b then sp else
      case elemIndex eq asms of
        Just i -> exact # fi i
        Nothing -> do
          if not p && is_ctor a && is_ctor b && not (same_ctor a b) then not_false else do
          asms <- get_asms
          maybe nop id # msum # flip map simp_rules #
            \(f, m) -> findIndex (\(Equality p a b) -> f p a b) asms >>= \i -> Just # m # fi i

simp_plus :: Proof ()
simp_plus = do
  simp
  tac_sort_goals

call :: [Expr] -> Expr
call = exprs_to_call

cref :: Name -> [Expr] -> Expr
cref name args = call # Ref name : args

have_expr :: Expr -> Proof ()
have_expr expr = have # mk_prop expr

have_n :: Expr -> Proof ()
have_n expr = have # mk_prop_n expr

focus_nth :: N -> Proof ()
focus_nth n = do
  goals <- gets _goals
  focus # goals !! fi n

split_and :: Proof ()
split_and = do
  Equality True (Call (Call (Call 0 a) 1) b) 0 <- get_goal_eq
  n <- get_asms_num
  have_expr a
  focus_nth 1
  have_expr b
  clear n
  focus_nth 1
  rw n
  rw (n + 1)
  simp

newtype Tactic = Tactic (Proof ())

instance Read Tactic where
  readPrec = parse_tac

parse_tac :: ReadPrec Tactic
parse_tac = trimmed # choice
  [ parse_tac_0 "unf" unf
  , parse_tac_0 "ite" split_ite
  , parse_tac_0 "ctr" contra
  , parse_tac_1n "unf" unf_at
  , parse_tac_1n "ite" cases_ite
  , parse_tac_1n "rw" rw
  , parse_tac_2n "rw" rw_at
  , parse_tac_1n "sub" subst
  , parse_tac_1n "ind" induct
  , parse_tac_1n "cpos" cpos
  , parse_tac_1n "clr" clear
  , parse_tac_have
  ]

parse_tac_0 :: Name -> Proof () -> ReadPrec Tactic
parse_tac_0 name tac = p_str name >> pure (Tactic tac)

parse_tac_1n :: Name -> (N -> Proof ()) -> ReadPrec Tactic
parse_tac_1n name tac = do
  p_str name
  n <- trimmed # parse_nat
  return # Tactic # tac n

parse_tac_2n :: Name -> (N -> N -> Proof ()) -> ReadPrec Tactic
parse_tac_2n name tac = do
  p_str name
  n1 <- trimmed # parse_nat
  n2 <- trimmed # parse_nat
  return # Tactic # tac n1 n2

parse_tac_have :: ReadPrec Tactic
parse_tac_have = do
  p_str "have"
  eq <- parse_eq
  return # Tactic # do
    goal <- get_goal
    let vars = _goal_vars goal
    have # put_vars_eq vars eq