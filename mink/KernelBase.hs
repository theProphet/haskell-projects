module KernelBase where
  
import Data.Char
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import GHC.Read
import Text.ParserCombinators.ReadPrec

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import ParserBase

reserved_chars :: String
reserved_chars = "()"

type Name = String

data Expr =
  Nil |
  Pair Expr Expr |
  Other Expr |
  Call Expr Expr |
  Ref Name |
  Var Name
  deriving Eq

is_pair :: Expr -> Prop
is_pair (Pair _ _) = True
is_pair _ = False

is_call :: Expr -> Prop
is_call (Call _ _) = True
is_call _ = False

is_nat :: Expr -> Prop
is_nat Nil = True
is_nat (Pair Nil a) = is_nat a
is_nat _ = False

nat_to_expr :: N -> Expr
nat_to_expr 0 = Nil
nat_to_expr n = Pair Nil (nat_to_expr # n - 1)

expr_to_nat :: Expr -> N
expr_to_nat Nil = 0
expr_to_nat (Pair Nil a) = expr_to_nat a + 1

lift_nat :: (N -> N) -> Expr -> Expr
lift_nat f a = nat_to_expr # f (expr_to_nat a)

lift_nat2 :: (N -> N -> N) -> Expr -> Expr -> Expr
lift_nat2 f a b = nat_to_expr # f (expr_to_nat a) (expr_to_nat b)

instance Num Expr where
  (+) = lift_nat2 # \a b -> abs # a + b
  (-) = lift_nat2 (-)
  (*) = lift_nat2 (*)
  abs = id
  signum = lift_nat signum
  fromInteger = nat_to_expr

instance Show Expr where
  show expr = if is_nat expr
    then show # expr_to_nat expr
    else case expr of
      Pair a b -> concat ["(", show a, ", ", show_pair b, ")"]
      Call a b -> unwords [show a, show_call b]
      Other a -> unwords ["@", show_call a]
      Ref name -> name
      Var name -> name

show_pair :: Expr -> String
show_pair a = let
  s = show a
  in if is_pair a then tail # init s else s

show_call :: Expr -> String
show_call a = let
  s = show a
  in if is_call a then surround_by_parens s else s

instance Read Expr where
  readPrec = parse_expr

parse_expr :: ReadPrec Expr
parse_expr = do
  ~(_, exprs) <- p_sep p_ws parse_term
  if null exprs then pfail else do
    return # foldl1 Call exprs

parse_term :: ReadPrec Expr
parse_term = choice
  [ parse_expr_nat
  , parse_ident
  , parse_tuple
  , parse_other
  ]

parse_expr_nat :: ReadPrec Expr
parse_expr_nat = do
  n <- parse_nat
  return # fi n

parse_ident :: ReadPrec Expr
parse_ident = do
  name <- parse_name
  if isDigit # head name then pfail else pure # Ref name

parse_tuple :: ReadPrec Expr
parse_tuple = do
  p_char '('
  exprs <- p_sep' parse_expr (p_char ',')
  p_char ')'
  if null exprs
    then return Nil
    else return # foldr1 Pair exprs

parse_other :: ReadPrec Expr
parse_other = do
  p_char '@'
  expr <- parse_expr
  return # Other expr

parse_name :: ReadPrec Name
parse_name = p_take_while1 is_ident_char

parse_nat :: ReadPrec N
parse_nat = do
  n <- p_take_while1 isDigit
  return (read n :: N)

parse_name_t :: ReadPrec Name
parse_name_t = trimmed parse_name

data Equality = Equality
  { _pos :: Prop
  , _lhs :: Expr
  , _rhs :: Expr
  } deriving Eq

instance Show Equality where
  show (Equality p a b) = if b == 0
    then ite p "" "~" ++ show a
    else unwords [show a, ite p "=" "/=", show b]

instance Read Equality where
  readPrec = parse_eq

parse_eq :: ReadPrec Equality
parse_eq = trimmed # choice
  [ parse_eq_pos
  , parse_eq_neg
  , parse_prop_pos
  , parse_prop_neg
  ]

parse_eq_pos :: ReadPrec Equality
parse_eq_pos = do
  lhs <- parse_expr
  p_char '='
  rhs <- parse_expr
  return # Equality True lhs rhs

parse_eq_neg :: ReadPrec Equality
parse_eq_neg = do
  lhs <- parse_expr
  p_str "/="
  rhs <- parse_expr
  return # Equality False lhs rhs

parse_prop_pos :: ReadPrec Equality
parse_prop_pos = do
  lhs <- parse_expr
  return # Equality True lhs Nil

parse_prop_neg :: ReadPrec Equality
parse_prop_neg = do
  p_char '~'
  lhs <- parse_expr
  return # Equality False lhs Nil

data Def = Def
  { _def_name :: Name
  , _def_arity :: N
  , _def_args_list :: [Name]
  , _def_args :: Map Name N
  , _body :: Expr
  , _is_rec :: Prop
  }

instance Show Def where
  show def = unwords
    [ unwords # _def_name def : _def_args_list def, "="
    , show # _body def
    ]

data Lemma = Lemma
  { _lemma_name :: Name
  , _lemma_args :: [Name]
  , _lemma_vars :: Set Name
  , _lemma_asms :: [Equality]
  , _lemma_goal_eq :: Equality
  }

instance Show Lemma where
  show lemma = unwords
    [ unwords # _lemma_name lemma : _lemma_args lemma, ":"
    , intercalate " -> " # map show # lemma_to_eqs lemma
    ]

init_proof :: TheoryT -> Lemma -> ProofT
init_proof th lemma = ProofT
  { _theory = th
  , _lemma = lemma
  , _goals = [lemma_to_goal lemma]
  }

lemma_to_goal :: Lemma -> Goal
lemma_to_goal lemma = Goal
  { _id = [0]
  , _goal_vars = _lemma_vars lemma
  , _asms = _lemma_asms lemma
  , _goal_eq = _lemma_goal_eq lemma
  }

lemma_to_eqs :: Lemma -> [Equality]
lemma_to_eqs lemma = _lemma_asms lemma ++ [_lemma_goal_eq lemma]

show_vars :: [Name] -> String
show_vars names = surround '{' '}' # unwords names

data Goal = Goal
  { _id :: [N]
  , _goal_vars :: Set Name
  , _asms :: [Equality]
  , _goal_eq :: Equality
  } deriving Eq

instance Ord Goal where
  goal1 <= goal2 = _id goal1 <= _id goal2

instance Show Goal where
  show goal = unlines' # concat
    [ [{-show_id # _id goal,-} show_vars # Set.toList # _goal_vars goal]
    , zipWith show_asm [0..] (_asms goal)
    , ["|- " ++ (show # _goal_eq goal)]
    ]

show_id :: [N] -> String
show_id xs = surround '[' ']' # intercalate ", " # map show xs

show_asm :: N -> Equality -> String
show_asm n eq = concat [show n, ". ", show eq]

data ProofT = ProofT
  { _theory :: TheoryT
  , _lemma :: Lemma
  , _goals :: [Goal]
  }

instance Show ProofT where
  show proof = let
    goals = _goals proof
    goals_num = length goals
    in unlines' #
      [ show # _lemma proof
      , concat [show # goals_num, " goal", if goals_num /= 1 then "s" else ""]
      ] ++ map (\goal -> "\n" ++ show goal) goals ++
      if null goals then ["\nGoals accomplished!"] else []

data TheoryT = TheoryT
  { _defs :: Map Name (Maybe Def)
  , _defs_list :: [(Name, Maybe Def)]
  , _lemmas :: Map Name Lemma
  , _proof :: Maybe ProofT
  }

instance Show TheoryT where
  show theory = unlines' # map (uncurry show_def) #
    filter (\(name, _) -> not # name >= "0" && name <= "9") #
      _defs_list theory

empty_theory :: TheoryT
empty_theory = TheoryT
  { _defs = Map.empty
  , _defs_list = []
  , _lemmas = Map.empty
  , _proof = Nothing
  }

show_def :: Name -> Maybe Def -> String
show_def name Nothing = concat [name, " = ???"]
show_def name (Just def) = show def

instance Read Lemma where
  readPrec = parse_lemma

parse_lemma :: ReadPrec Lemma
parse_lemma = do
  name <- parse_name_t
  args <- p_all parse_name_t
  let vars = Set.fromList args
  p_char ':'
  eqs_raw <- p_sep' parse_eq (p_str "->")
  if null eqs_raw then pfail else do
    let eqs = map (put_vars_eq vars) eqs_raw
    return # Lemma
      { _lemma_name = name
      , _lemma_args = args
      , _lemma_vars = vars
      , _lemma_asms = init eqs
      , _lemma_goal_eq = last eqs
      }

put_vars_eq :: Set Name -> Equality -> Equality
put_vars_eq vars (Equality p a b) = Equality p
  (put_vars_expr vars a) (put_vars_expr vars b)

put_vars_expr :: Set Name -> Expr -> Expr
put_vars_expr args expr = case expr of
  Pair a b -> Pair (put_vars_expr args a) (put_vars_expr args b)
  Other a -> Other (put_vars_expr args a)
  Call a b -> Call (put_vars_expr args a) (put_vars_expr args b)
  Ref name -> ite (Set.member name args) (Var name) expr
  _ -> expr