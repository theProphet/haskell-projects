import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Text.Read
import System.IO

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import Kernel
import Tactic
import Def

src_dir = "src/"
defs_file = src_dir ++ "defs.txt"
proofs_file = src_dir ++ "proofs.txt"

main :: IO ()
main = do
  mapM_ (flip hSetBuffering NoBuffering) [stdin, stdout, stderr]
  defs_src <- readFile defs_file
  proofs_src <- readFile proofs_file
  th <- repl # build_theory # parse_and_apply_defs defs_src
  nop

repl :: TheoryT -> IO TheoryT
repl th = do
  lemma <- read_lemma
  case lemma of
    Nothing -> do
      putStrLn "Syntax error"
      repl th
    Just lemma -> do
      let proof = init_proof th lemma
      proof <- pure # apply_tac simp_plus proof
      tacs <- proof_mode [] proof
      return th
      -- repl th

proof_mode :: [Proof ()] -> ProofT -> IO [Proof ()]
proof_mode tacs proof = do
  putStrLn ""
  print proof
  if null # _goals proof
    then return # reverse tacs
    else do
      tac <- read_tac
      case tac of
        Nothing -> do
          putStrLn "Unknown tactic"
          proof_mode tacs proof
        Just (Tactic tac) -> do
          proof <- pure # apply_tac (tac >> simp_plus) proof
          proof_mode (tac : tacs) proof

read_tac :: IO (Maybe Tactic)
read_tac = do
  line <- read_line
  return # readMaybe line

read_lemma :: IO (Maybe Lemma)
read_lemma = do
  line <- read_line
  return # readMaybe line

read_line :: IO String
read_line = do
  putStr "\n>>> "
  getLine

clear_screen :: IO ()
clear_screen = do
  putStr "\ESC[2J\ESC[0;0H\ESCc"