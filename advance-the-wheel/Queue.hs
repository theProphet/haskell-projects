module Queue where

import qualified Data.Sequence as Seq
import Data.Sequence (Seq)

import Base

type Queue = Seq Bit

q_mk :: Bits -> Queue
q_mk = Seq.fromList

q_empty :: Queue
q_empty = q_mk []

q_null :: Queue -> Prop
q_null = Seq.null

q_push :: Bit -> Queue -> Queue
q_push = flip (Seq.|>)

q_pop :: Queue -> (Bit, Queue)
q_pop q = (Seq.index q 0, Seq.drop 1 q)

q_bits :: Queue -> Bits
q_bits = foldr (:) []